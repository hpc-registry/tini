ARG IMAGE_TAG=24.04@sha256:72297848456d5d37d1262630108ab308d3e9ec7ed1c3286a32fe09856619a782
FROM ubuntu:${IMAGE_TAG}

# renovate: datasource=repology depName=ubuntu_24_04/tini versioning=semver-coerced
ARG TINI_VERSION=0.19.0-1
ARG CREATED=unknown
ARG REVISION=unknown
ARG IMAGE_TAG=24.04

RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install --no-install-recommends -y tini="${TINI_VERSION}" \
 && rm -rf /var/cache/apt/

ENTRYPOINT ["/usr/bin/tini", "/bin/bash", "--"]

LABEL org.opencontainers.image.source="https://gitlab.ethz.ch/hpc-registry/tini"
LABEL org.opencontainers.image.url="https://gitlab.ethz.ch/hpc-registry/tini"
LABEL org.opencontainers.image.revision="${REVISION}"
LABEL org.opencontainers.image.base="docker.io/ubuntu:${IMAGE_TAG}"
LABEL org.opencontainers.image.authors="Michal Minář <michal.minar@id.ethz.ch>"
LABEL org.opencontainers.image.licenses="AGPL-3.0-or-later"
LABEL org.opencontainers.image.created="${CREATED}"
