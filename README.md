# Base container images with tini init

From [tini](https://github.com/krallin/tini):

> Tini is the simplest init you could think of.
>
> All Tini does is spawn a single child (Tini is meant to be run in a
> container), and wait for it to exit all the while reaping zombies and
> performing signal forwarding.

Useful if all that you need in your container (apart from core utilities and a custom
entrypoint) script is just an init process reaping zombies.

## Images ready for consumption

At least the following shall be available and up-to-date on every-day basis:

- registry.ethz.ch/hpc-registry/tini/ubuntu:24.04
- registry.ethz.ch/hpc-registry/tini/ubuntu:22.04
- registry.ethz.ch/hpc-registry/tini/alpine:3
- registry.ethz.ch/hpc-registry/tini/alpine:3-bash
- registry.ethz.ch/hpc-registry/tini/alpine:3-ash

For additional tags, check out the following output:

```sh
# for Ubuntu based images
skopeo inspect docker://registry.ethz.ch/hpc-registry/tini/ubuntu:latest \
    | jq -r '.RepoTags[]' | sort -V
# for Alpine based images
skopeo inspect docker://registry.ethz.ch/hpc-registry/tini/ubuntu:latest \
    | jq -r '.RepoTags[]' | sort -V
```

## Example

See [example](./example/).
