#!/usr/bin/env bash

set -euo pipefail

while true; do
    printf 'Starting a watch for ConfigMap/%s ...\n' "$CONFIGMAP"
    kubectl get -w "cm/$CONFIGMAP" | while IFS=  read -r _; do
        printf 'ConfigMap/%s changed\n' "$CONFIGMAP"
        kubectl get --show-labels "cm/$CONFIGMAP"
    done
    sleep 1
done
