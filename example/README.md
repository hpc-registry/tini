# cm-watcher tini example

Does nothing useful. It just logs a message whenever a configmap changes.

Requirements:

- `kubectl` installed on at `/usr/bin/kubectl` on every compute node where the
  pod can get scheduled
- `kubectl` and `helm` in your `$PATH`

## Deploy

If you don't have an existing ConfigMap that you want to watch, feel free to
deploy as is in an arbitrary namespace:

```sh
NAMESPACE=cm-watcher
CM_NAME=cm-watcher-script
helm install --namespace $NAMESPACE --create-namespace cm-watcher . -f values.yaml
```

Or supply an existing configmap in an existing namespace:

```sh
NAMESPACE=kube-system
CM_NAME=cm-watcher-script
helm install --namespace $NAMESPACE cm-watcher . --set-string=configMap.name=$CM_NAME
```

Watch for cm changes:

```sh
# wait for ready
kubectl wait -n "$NAMESPACE" --for=condition=Ready deploy/cm-watcher
# watch logs
kubectl logs -n "$NAMESPACE" --follow -n cm-watcher deploy/cm-watcher
```

In another terminal, update the cm:

```sh
kubectl label -n "$NAMESPACE" cm/"$CM_NAME" foo=bar
```

Check the `kubectl logs`' output in the first terminal.

## Cleanup

```sh
helm uninstall -n "$NAMESPACE" cm-watcher
```
